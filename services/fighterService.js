const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    search(data) {
        const item = FighterRepository.getOne(data)
        if(!item) {
            return null
        }
        return item
    }
    
    getAll() {
        const fighters = FighterRepository.getAll()
        if(!fighters) {
            return null
        } 
        return fighters.map(fighter => ({
            name: fighter.name,
            health: fighter.health,
            power: fighter.power,
            defense: fighter.defense
        }))
    }
    
    create(data) {
        const fighter = FighterRepository.create(data)
        if(!fighter) {
            return null
        }
        return fighter
    }
    
    updateFighter(id, data) {
        const fighter = FighterRepository.update(id, data)
        if(!fighter) {
            return null
        } 
        return fighter
    }
    
    deleteFighter(id) {
        const item = FighterRepository.delete(id)
        if(!item) {
            return null
        } 
        return item
    }
    
}

module.exports = new FighterService();