const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    getAll() {
        const users = UserRepository.getAll()
        if(!users) {
            return null
        }
        return users.map(user => ({
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            phoneNumber: user.phoneNumber
        }))
    }
    
    create(data) {
        const user = UserRepository.create(data)
        if(!user) {
            return null;
        }
        return user;
    }
    
    updateUser(id, data) {
        const user = UserRepository.update(id, data)
        if(!user) {
            return null
        } 
        return user
    }
    
    deleteUser(id) {
        const item = UserRepository.delete(id)
        if(!item) {
            return null
        }
        return item
    }
}

module.exports = new UserService();