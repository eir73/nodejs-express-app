const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
//CRUD for fights
router.post('/', (req, res, next) => {
    res.send('post fights')
})
router.get('/', (req, res, next) => {
    res.send('get fights')
})

router.put('/', (req, res, next) => {
    res.send('put fights')
})

router.delete('/', (req, res, next) => {
    res.send('delete fights')
})


module.exports = router;