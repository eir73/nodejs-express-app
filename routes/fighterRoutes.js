const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.post('/', createFighterValid, (req, res, next) => {
    try {
        req.body.defense = Math.floor(Math.random() * 4 + 1) 
        //defense нет в теле запроса, поэтому 
        //приходится делать вот так
        const fighter = FighterService.create(req.body)
        if(fighter) {
            res.data = fighter
        } else {
            throw err
        }
    } catch (err) {
        res.data = {
            status: 400,
            error: true,
            message: 'This fighter has already exists'
        }
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/', (req, res, next) => {
    try {
        const fighters = FighterService.getAll()
        if(fighters[0]) {
            res.data = fighters
        } else {
            throw err
        }
    } catch (err) {
        res.data = {
            status: 400,
            error: true,
            message: 'No fighters'
        }
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
    try {
        const fighter = FighterService.search({id: req.params.id})
        if(fighter) {
            res.data = fighter
        } else {
            throw err
        }
    } catch(err) {
        res.data = {
            status: 404,
            error: true,
            message: 'No fighter with this id'
        }
    } finally {
        next()
    }
}, responseMiddleware)


router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        const fighter = FighterService.search({id: req.params.id})
        if(fighter) {
            const newFighter = FighterService.updateFighter(req.params.id, req.body)
            res.data = newFighter
        } else {
            throw err
        }
    } catch(err) {
        res.data = {
            status: 404,
            error: true,
            message: 'No fighter with this id'
        }
    } finally {
        next()
    }
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
    try {
        const fighter = FighterService.search({id: req.params.id})
        if(user) {
            res.data = FighterService.deleteFighter({id: req.params.id})
        } else {
            throw err
        }
    } catch(err) {
        res.data = {
            status: 404,
            error: true,
            message: 'No fighter with this id'
        }
    } finally {
        next()
    }
}, responseMiddleware)

module.exports = router;