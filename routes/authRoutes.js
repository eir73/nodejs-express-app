const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const data = AuthService.login(req.query)
        if(data) {
            res.data = {
                status: 200,
                error: false,
                message: 'You are logged in'
            }
        }
    } catch (err) {
        res.data = {
            status: 400,
            error: true,
            message: 'Failed to login'
        }
    } finally {
        next();
    }
}, responseMiddleware);


router.get('/', (req, res) => {
    res.send('ok')
})
module.exports = router;