const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.post('/', createUserValid, (req, res, next) => {
    try {
        const user = UserService.create(req.body);
    if(user) {
        res.data = user
    } else {
        throw err
    } 
    } catch(err) {
        res.data = {
            status: 400,
            error: true,
            message: 'This user has already exists'
        }
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/', (req, res, next) => {
    try {
        const users = UserService.getAll()
        if(users[0]){
            res.data = users
        } else {
            throw err
        }
    } catch(err) {
        res.data = {
            status: 404,
            error: true,
            message: 'No users'
        }
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
    try {
        const user = UserService.search({id: req.params.id})
        if(user) {
            res.data = user
        } else {
            throw err
        }
    } catch(err) {
        res.data = {
            status: 404,
            error: true,
            message: 'No user with this id'
        }
    } finally {
        next()
    }
}, responseMiddleware)

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        const user = UserService.search({id: req.params.id})
        if(user) {
            const newUser = UserService.updateUser(req.params.id, req.body)
            res.data = newUser
        } else {
            throw err
        }
    } catch(err) {
        res.data = {
            status: 404,
            error: true,
            message: 'No user with this id'
        }
    } finally {
        next()
    }
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
    try {
        const user = UserService.search({id: req.params.id})
        if(user) {
            res.data = UserService.deleteUser({id: req.params.id})
        } else {
            throw err
        }
    } catch(err) {
        res.data = {
            status: 404,
            error: true,
            message: 'No user with this id'
        }
    } finally {
        next()
    }
}, responseMiddleware)

module.exports = router;