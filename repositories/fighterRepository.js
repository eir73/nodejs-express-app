const { BaseRepository } = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }
    
    create(data) {
        const fighterPrototype = super.create(data);
        fighterPrototype.name = data.name;
        fighterPrototype.health = 100;
        fighterPrototype.power = data.power;
        fighterPrototype.defense = data.defense;
        
        return fighterPrototype
    }
}

exports.FighterRepository = new FighterRepository();