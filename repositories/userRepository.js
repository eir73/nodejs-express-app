const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }
    
    create(data) {
        const userPrototype = super.create(data);
        userPrototype.firstName = data.firstName;
        userPrototype.lastName = data.lastName;
        userPrototype.email = data.email;
        userPrototype.phoneNumber = data.phoneNumber;
        userPrototype.password = data.password;
        
        return userPrototype;
    }
}

exports.UserRepository = new UserRepository();