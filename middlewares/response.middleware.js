const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if(res.data.error) {
            res.statusCode = res.data.status
        res.send({
            error: res.data.error,
            message: res.data.message
        })
    } else {
            res.statusCode = 200
        res.send(res.data)
    }
        
        
}

exports.responseMiddleware = responseMiddleware;