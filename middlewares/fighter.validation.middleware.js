const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if(req.body.name && req.body.power && req.body.power < 100) {
        res.data = req.body
    } else {
        res.data = {
            status: 400,
            error: true,
            message: 'Fighter entity to create isn\'t valid'
        }
    }
    next()
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if(req.body.name && req.body.power && req.body.power < 100) {
        res.data = req.body
    } else {
        res.data = {
            status: 400,
            error: true,
            message: 'Fighter entity to update isn\'t valid'
        }
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;