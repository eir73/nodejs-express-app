const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    if(req.body.firstName && req.body.lastName && (req.body.email.substring(req.body.email.length - 10) == '@gmail.com' && req.body.phoneNumber.substring(0, 4) == '+380' && req.body.phoneNumber.length == 13 && req.body.password.length >= 3)) {
        res.data = req.body
   } else {
       res.data = {
           status: 400,
           error: true,
           message: 'User entity to create isn\'t valid'
       }
   }
    next()
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if(req.body.firstName && req.body.lastName && (req.body.email.substring(req.body.email.length - 10) == '@gmail.com' && req.body.phoneNumber.substring(0, 4) == '+380' && req.body.phoneNumber.length == 13 && req.body.password.length >= 3)) {
        res.bata = req.body
   } else {
       res.data = {
           status: 400,
           error: true,
           message: 'User entity to update isn\'t valid'
       }
   }
    next()
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;